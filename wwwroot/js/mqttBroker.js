﻿class log {
    static success = msg => {
        console.log('%c' + msg, 'color: #080');
    }
    static error = msg => {
        console.error(msg);
    }
}

class broker {
    static count = 0;
    static connection;

    static connect() {
        this.connection = new signalR.HubConnectionBuilder()
            .withUrl("/broker")
            .build();
        this.connection.on("Request", function (message) {
        });
        this.connection.on("Response", function (message) {
            const parsedData = jQuery.parseJSON(message);
            const additionalData = parsedData.features[0].properties.summary;
            //console.log(additionalData);

            $("#duree").text(Math.round(additionalData.duration / 60) + "min");
            $("#distance").text(getDistance(additionalData.distance));

            $(".leaflet-zoom-animated g .leaflet-interactive").remove();
            L.geoJSON(jQuery.parseJSON(message)).addTo(mymap);
        });
        this.connection.start().catch(function (err) {
            return console.error(err.toString());
        });
    }

    static updateState() {
        return;
    }

    static emit(coords) {
        this.connection.invoke("Request", JSON.stringify(coords)).catch(function (err) {
            return console.error(err.toString());
        });
    }

    static test(e) {
        e.preventDefault();
        let testCoords = {};
        testCoords.startlat = 45.7505926;
        testCoords.startLong = 4.8113262;
        testCoords.stopLat = 45.7380158;
        testCoords.stopLong = 4.8680768;
        testCoords.TransportType = "all";
        this.connection.invoke("Request", JSON.stringify(testCoords)).catch(function (err) {
            return console.error(err.toString());
        });
        return false;
    }
}

function getDistance(distance) {
    if (distance < 1000) {
        return distance.toFixed(0) + "m";
    } else {
        return (distance / 1000).toFixed(1) + "km";
    }
}


$(document).ready(_ => {
    broker.connect();
    $(document).on('click', '#test', function (e) {
        e.preventDefault();

        broker.test(e);

        $(this).hide();
        setTimeout(_ => {
            $(this).show();
        }, 1000);
    });
});
